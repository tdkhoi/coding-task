//
//  CoreDataHelper.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "User.h"
#import "Post.h"
#import "UIUser.h"
#import "UIPost.h"

@interface CoreDataHelper : NSObject

@end
