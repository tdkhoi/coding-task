//
//  APIRequestGetCommentCount.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/22/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "APIRequestGetCommentCount.h"

@implementation APIRequestGetCommentCount

- (NSString *)getRequestPath {
    return [NSString stringWithFormat:@"/post/%@/commentCount", _postId];
}



@end
