//
//  APIRequestGetLikeCount.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/21/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "SFAPIRequest.h"


@interface APIRequestGetLikeCount : SFAPIRequest

@property (nonatomic, strong) NSString* postId;

@end
