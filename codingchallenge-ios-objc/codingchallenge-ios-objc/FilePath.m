//
//  FilePath.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "FilePath.h"

static NSString *bundlePathString = nil;
static NSString *rootPathString = nil;

@implementation FilePath

+ (NSString*)bundlePath {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(bundlePathString == nil) bundlePathString = [[[NSBundle mainBundle] resourcePath] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"/%@", [[[NSBundle mainBundle] resourcePath] lastPathComponent]] withString:@""];
    });
    
    
    return bundlePathString;
}

+ (NSString*)appName{
    return [[[NSBundle mainBundle] resourcePath] lastPathComponent];
}

+ (NSString*)rootPath{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(rootPathString == nil){
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            rootPathString = [paths objectAtIndex:0];
            
        }
    });
    
    
    return rootPathString;
}

+ (NSString*)createDirectoryWithName:(NSString*)name{
    
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    NSString *path = [[FilePath rootPath] stringByAppendingPathComponent:name];
    
    if(![fileManager fileExistsAtPath:path]){
        
        NSError *error = nil;
        if(![fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Error when create \" %@ \" directory", name);
            
        }
        [self addSkipBackupAttributeToItemAtPath:path];
    }
    
    return path;
}

+ (NSString*)databasePath
{
    return [FilePath createDirectoryWithName:@"Database"];
}

+ (BOOL)isFileExisted:(NSString *)path
{
    NSFileManager *fileManager= [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:path];
}

+ (BOOL)isFolderExisted:(NSString *)path
{
    BOOL isDir;
    NSFileManager *fileManager= [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path isDirectory:&isDir]) {
        return isDir;
    }
    return FALSE;
}

+ (BOOL)deleteFolder:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:path error:&error];
    return success;
}

+ (BOOL)deleteFile:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:path error:&error];
    return success;
}

+ (BOOL)copyAllFileFromPath:(NSString *)source toPath:(NSString *)destination
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:destination]) {
        [fileManager removeItemAtPath:destination error:nil];
    }
    return [[NSFileManager defaultManager] copyItemAtPath:source toPath:destination error:nil];
}

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
@end
