//
//  SFAPIRequest.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SFAPIRequestProtocol <NSObject>

- (NSString *)getRequestPath;
- (NSString *)getRequestMethod;

@end

@interface SFAPIRequest : NSObject <SFAPIRequestProtocol>

@end
