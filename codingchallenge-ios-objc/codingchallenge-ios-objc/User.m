//
//  User.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic id_m;
@dynamic dob;
@dynamic fullName;
@dynamic mobileNo;
@dynamic imageURL;

@end
