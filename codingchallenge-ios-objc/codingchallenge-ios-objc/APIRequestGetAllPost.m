//
//  APIRequestGetAllPost.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "APIRequestGetAllPost.h"

@implementation APIRequestGetAllPost

- (NSString *)getRequestPath {
    return @"/post";
}
@end
