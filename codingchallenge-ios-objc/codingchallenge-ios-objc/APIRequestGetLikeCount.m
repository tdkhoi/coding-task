//
//  APIRequestGetLikeCount.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/21/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "APIRequestGetLikeCount.h"

@implementation APIRequestGetLikeCount

- (NSString *)getRequestPath {
    return [NSString stringWithFormat:@"/post/%@/likeCount", _postId];
}

@end
