//
//  Post.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "Post.h"

@implementation Post

@dynamic id_m;
@dynamic commentCount;
@dynamic likeCount;
@dynamic message;
@dynamic postAt;
@dynamic user;
@dynamic imageURL;


@end
