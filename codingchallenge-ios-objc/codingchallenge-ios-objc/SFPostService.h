//
//  SFPostService.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/21/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SFPostServiceProtocol <NSObject>

- (void)getAllPostFromServer;
- (void)getLikeCountFromPost:(NSString *)postId;

- (NSArray *)getAllPost;

@end

@interface SFPostService : NSObject <SFPostServiceProtocol>

@end
