//
//  Post.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/22/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "UIPost.h"

@implementation UIPost

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.id_m = dict[@"_id"];
        self.imageURL = dict[@"imageURL"];
        self.message = dict[@"message"];
        self.postAtString = dict[@"postedAt"];
        self.likeCount = 0;
        self.commentCount = 0;
        self.user = [[UIUser alloc] initWithDictionary:dict[@"user"]];
        
        
    }
    return self;
}


@end
