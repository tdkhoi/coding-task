//
//  Post.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import "User.h"

@interface Post : NSManagedObject

@property (retain, nonatomic) NSString *id_m;
@property (retain, nonatomic) NSString *imageURL;
@property (retain, nonatomic) NSString *message;
@property (retain, nonatomic) NSString *postAt;
@property (nonatomic) int16_t commentCount;
@property (nonatomic) int16_t likeCount;
@property (retain, nonatomic) User *user;





@end
