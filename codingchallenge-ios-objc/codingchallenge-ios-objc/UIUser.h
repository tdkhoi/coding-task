//
//  UIUser.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIUser : NSObject

@property (strong, nonatomic) NSString *id_m;
@property (strong, nonatomic) NSString *dobString;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *phoneNumber;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
