//
//  SFBinding.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "SFBinding.h"
#import "SFPostService.h"

static NSMutableDictionary *key_object = nil;

@implementation SFBinding

+ (void)initOrReset {
    key_object = [NSMutableDictionary new];
    // congfig
    [self bindSingle:@protocol(SFPostServiceProtocol) withObject:[SFPostService new]];
    
}


+ (void)bindSingle:(Protocol *)protocol withObject:(id)object {
    if ([object conformsToProtocol:protocol]) {
        NSString *key = NSStringFromProtocol(protocol);
        [key_object setObject:object forKey:key];
    } else {
#if DEBUG
        NSAssert(FALSE, @"Bind failed, obj is not conformed to protocol");
#endif
    }
}
+ (id)objectSingleForProtocol:(Protocol *)protocol
{
    return [key_object objectForKey:NSStringFromProtocol(protocol)];
}



@end
