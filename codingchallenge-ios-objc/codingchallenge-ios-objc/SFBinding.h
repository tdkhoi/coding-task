//
//  SFBinding.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SFBindGetSingleForProtocol(X) (id<X>)[SFBinding objectSingleForProtocol:@protocol(X)]

@interface SFBinding : NSObject

+ (void)initOrReset;
+ (void)bindSingle:(Protocol *)protocol withObject:(id)object;
+ (id)objectSingleForProtocol:(Protocol *)protocol;


@end
