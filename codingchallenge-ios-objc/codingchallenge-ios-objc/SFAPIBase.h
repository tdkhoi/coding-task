//
//  SFAPIBase.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFAPIRequest.h"
#import "SFAPIResponse.h"
#import "SFAPIDefine.h"

@interface SFAPIBase : NSObject

- (void)setAPIResponseType:(Class<SFAPIResponseProtocol>)cla;
- (void)setRequest:(id<SFAPIRequestProtocol>)req;
- (void)performRequestWithSuccess:(void (^)(id<SFAPIResponseProtocol> response))successCallback error:(void (^)(int errorCode, NSString *err)) errorCallback;
@end
