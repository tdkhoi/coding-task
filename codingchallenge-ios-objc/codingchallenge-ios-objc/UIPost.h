//
//  Post.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/22/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIUser.h"

@interface UIPost : NSObject

@property (strong, nonatomic) NSString *id_m;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *message;
@property (assign, nonatomic) NSInteger likeCount;
@property (assign, nonatomic) NSInteger commentCount;
@property (strong, nonatomic) NSString *postAtString;
@property (strong, nonatomic) UIUser *user;


- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end
