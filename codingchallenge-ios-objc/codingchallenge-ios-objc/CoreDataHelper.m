//
//  CoreDataHelper.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "CoreDataHelper.h"
#import "FilePath.h"

@interface CoreDataHelper() {
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectContext *managedObjectContext;
}

@end

@implementation CoreDataHelper

- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel)
    {
        return managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Social" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

- (BOOL)addPersistentStore:(NSError **)errorPtr
{
    NSURL *url = [NSURL fileURLWithPath:[CoreDataHelper DBFilePath]];
    NSPersistentStore *result = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                         configuration:nil
                                                                                   URL:url
                                                                               options:nil
                                                                                 error:errorPtr];
    return (result != nil);
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator)
    {
        return persistentStoreCoordinator;
    }
    
    NSManagedObjectModel *mom = [self managedObjectModel];
    if (mom == nil)
    {
        return nil;
    }
    
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    
    NSError *error = nil;
    if ([self addPersistentStore:&error] == NO)
    {
        persistentStoreCoordinator = nil;
    }
    
    return persistentStoreCoordinator;
}

- (void)createManagedObjectContext
{
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator)
    {
        if (managedObjectContext == nil)
        {
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [managedObjectContext setPersistentStoreCoordinator:coordinator];
            [managedObjectContext setMergePolicy:NSOverwriteMergePolicy];
        }
    }
}

+ (NSString *)DBFilePath
{
    return [[FilePath databasePath] stringByAppendingPathComponent:@"Social.sqlite"];
}

- (NSArray *)findAllForEntity:(NSString *)entityName
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSArray *postModels = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    return postModels;
}
- (Post *)savePost:(UIPost *)uiPost
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Post"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id_m LIKE %@", [uiPost id_m]];
    [fetchRequest setPredicate:predicate];
    Post *post = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];
    
    if (post == nil) {
        post = [NSEntityDescription insertNewObjectForEntityForName:@"Post" inManagedObjectContext:managedObjectContext];
    }
    
    [post setId_m:[uiPost id_m]];
    [post setMessage:[uiPost message]];
    [post setImageURL:[uiPost imageURL]];
    [post setPostAt:[uiPost postAtString]];
    [post setLikeCount:[uiPost likeCount]];
    [post setCommentCount:[uiPost commentCount]];
    [post setUser:[self saveUser:[uiPost user]]];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return nil;
    }
    
    return post;
}

- (User *)saveUser:(UIUser *)uiUser
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id_m LIKE %@", [uiUser id_m]];
    [fetchRequest setPredicate:predicate];
    User *user = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];
    
    if (user == nil) {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];
    }
    
    [user setId_m:[uiUser id_m]];
    [user setFullName:[uiUser fullName]];
    [user setDob:[uiUser dobString]];
    [user setMobileNo:[uiUser phoneNumber]];
    [user setImageURL:[uiUser imageURL]];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return nil;
    }
    
    return user;
}

- (Post *)updateForPostId: (NSString *)postId likeCount: (NSInteger)likeCount andCommentCount: (NSInteger)commentCount
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Post"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id_m LIKE %@", postId];
    [fetchRequest setPredicate:predicate];
    Post *post = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];
    
    [post setLikeCount:likeCount];
    [post setCommentCount:commentCount];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    return post;
}


@end
