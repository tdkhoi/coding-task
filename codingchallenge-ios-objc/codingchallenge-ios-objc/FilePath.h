//
//  FilePath.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilePath : NSObject

+ (NSString*)databasePath;

@end
