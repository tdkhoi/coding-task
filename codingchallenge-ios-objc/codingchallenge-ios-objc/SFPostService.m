//
//  SFPostService.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/21/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "SFPostService.h"
#import "SFAPIBase.h"
#import "APIResponseGetAllPost.h"
#import "APIRequestGetAllPost.h"
#import "APIRequestGetLikeCount.h"
#import "APIResponseGetLikeCount.h"
#import "APIRequestGetCommentCount.h"
#import "APIResponseGetCommentCount.h"

@interface SFPostService () {
    NSArray * _allPost;
    
}


@end

@implementation SFPostService

- (void)getAllPostFromServer {
    
    SFAPIBase *apiBase = [SFAPIBase new];
    [apiBase setRequest:[APIRequestGetAllPost new]];
    [apiBase setAPIResponseType:[APIResponseGetAllPost class]];
    
    [apiBase performRequestWithSuccess:^(id<SFAPIResponseProtocol> response) {
        APIResponseGetAllPost *getAllPost = (APIResponseGetAllPost *)response;
        
        
    } error:^(int errorCode, NSString *err) {
        
    }];
    
}


- (void)getLikeCountFromPost:(NSString *)postId {
    if (postId != nil) {
        SFAPIBase *apiBase = [SFAPIBase new];
        APIRequestGetLikeCount *request = [APIRequestGetLikeCount new];
        request.postId = postId;
        [apiBase setRequest:request];
        [apiBase setAPIResponseType:[APIResponseGetLikeCount class]];
        
        [apiBase performRequestWithSuccess:^(id<SFAPIResponseProtocol> response) {
            
        } error:^(int errorCode, NSString *err) {
            
        }];
    }
}

- (void)getCommentCountFromPost:(NSString *)postId {
    if (postId != nil) {
        SFAPIBase *apiBase = [SFAPIBase new];
        APIRequestGetCommentCount *request = [APIRequestGetCommentCount new];
        request.postId = postId;
        [apiBase setRequest:request];
        [apiBase setAPIResponseType:[APIResponseGetCommentCount class]];
        [apiBase performRequestWithSuccess:^(id<SFAPIResponseProtocol> response) {
            
        } error:^(int errorCode, NSString *err) {
            
        }];
    } else {
        
    }
    
}


- (NSArray *)getAllPost {
    
    return _allPost;
}
@end
