//
//  SFAPIDefine.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#ifndef SFAPIDefine_h
#define SFAPIDefine_h

#define API_RERTY 2

#define APP_HTTP_METHOD     @"http"
#define APP_DOMAIN_HTTP     @"thedemoapp.herokuapp.com"


#define API_CODE_SUCCESS    200
#define API_STATUS_SUCCESS  @"success"

#endif /* SFAPIDefine_h */
