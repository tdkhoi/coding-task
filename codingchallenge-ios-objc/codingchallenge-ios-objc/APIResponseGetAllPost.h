//
//  APIResponseGetAllPost.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/18/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "SFAPIResponse.h"

@interface APIResponseGetAllPost : SFAPIResponse

- (NSArray *)getAllPost;

@end
