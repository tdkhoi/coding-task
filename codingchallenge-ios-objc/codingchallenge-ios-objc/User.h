//
//  User.h
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface User : NSManagedObject

@property (retain, nonatomic) NSString *id_m;
@property (retain, nonatomic) NSString *dob;
@property (retain, nonatomic) NSString *fullName;
@property (retain, nonatomic) NSString *imageURL;
@property (retain, nonatomic) NSString *mobileNo;



@end
