//
//  SFAPIBase.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/17/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "SFAPIBase.h"

@interface SFAPIBase()
@property (strong, nonatomic) id<SFAPIRequestProtocol> request;
@property (strong, nonatomic) NSString *response;
@property (nonatomic) Class<SFAPIResponseProtocol> responseType;

@end

@implementation SFAPIBase


- (void)setAPIResponseType:(Class<SFAPIResponseProtocol>)cla {
    _responseType = cla;
}

- (void)setRequest:(id<SFAPIRequestProtocol>)req {
    _request = req;
}

- (Class)getAPIResponseType
{
    return _responseType;
}

- (void)performRequestWithSuccess:(void (^)(id<SFAPIResponseProtocol> response))successCallback error:(void (^)(int errorCode, NSString *err)) errorCallback {
    [self performRequestForTimes:API_RERTY success:successCallback error:errorCallback];
}


- (void)performRequestForTimes:(int)retryTimes success:(void (^)(id<SFAPIResponseProtocol> response))successCallback error:(void (^)(int errorCode, NSString *err)) errorCallback {
    if (_request) {
        NSString *url = [NSString stringWithFormat:@"%@://%@%@", APP_HTTP_METHOD, APP_DOMAIN_HTTP, [_request getRequestPath]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:[_request getRequestMethod]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error || data == nil) {
                if (retryTimes >= 0) {
                    [self performRequestForTimes:retryTimes-1 success:successCallback error:errorCallback];
                } else {
                    errorCallback((int)error.code, error.debugDescription);
                }
            } else {
                NSError *jsonError = nil;
                id object = [NSJSONSerialization
                             JSONObjectWithData:data
                             options:0
                             error:&jsonError];
                if (jsonError) {
                    if (retryTimes >= 0) {
                        [self performRequestForTimes:retryTimes-1 success:successCallback error:errorCallback];
                    } else {
                        errorCallback((int)jsonError.code, jsonError.debugDescription);
                    }
                } else {
                    if ([object isKindOfClass:[NSDictionary class]]) {
                        NSInteger code = [[object objectForKey:@"code"] integerValue];
                        if (code == API_CODE_SUCCESS) {
                            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[object objectForKey:@"data"],@"data", nil];
                            
                            id<SFAPIResponseProtocol> apiResponse = [[[self getAPIResponseType] alloc] initWithDictionary:dict];
                            successCallback(apiResponse);
                            
                        } else {
                            errorCallback((int)code, @"Request api error");
                        }
                        
                    } else {
                        if (retryTimes >= 0) {
                            [self performRequestForTimes:retryTimes-1 success:successCallback error:errorCallback];
                        } else {
                            errorCallback(0, @"Parse json error");
                        }
                    }
                }
            }
            
        }] resume];
        
    }
}
@end
