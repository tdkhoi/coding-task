//
//  UIUser.m
//  codingchallenge-ios-objc
//
//  Created by Tran Dinh Khoi on 5/25/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "UIUser.h"

@implementation UIUser

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.id_m = dict[@"_id"];
        self.dobString = dict[@"dob"];
        self.fullName = dict[@"fullName"];
        self.imageURL = dict[@"imageURL"];
        self.phoneNumber = dict[@"mobileNo"];
    }
    return self;
}


@end
